import 'package:diyagram/app/binding/comment_binding.dart';
import 'package:diyagram/app/binding/homebinding.dart';
import 'package:diyagram/app/binding/post_binding.dart';
import 'package:diyagram/app/binding/start_binding.dart';
import 'package:diyagram/app/view/comment.dart';
import 'package:diyagram/app/view/home.dart';
import 'package:diyagram/app/view/post.dart';
import 'package:diyagram/app/view/splash_screen.dart';
import 'package:get/get_navigation/src/routes/get_route.dart';

List<GetPage> pages = [
  GetPage(name: "/", page: () => const SplashScreen(), binding: StartBinding()),
  GetPage(name: "/home", page: () => const Home(), binding: HomeBinding()),
  GetPage(name: "/post", page: () => const Post(), binding: PostBinding()),
  GetPage(
      name: "/comment", page: () => const Comment(), binding: CommentBinding()),
];
