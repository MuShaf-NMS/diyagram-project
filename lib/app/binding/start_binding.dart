import 'package:diyagram/app/controller/connection.dart';
import 'package:diyagram/app/controller/splash_screen.dart';
import 'package:get/get.dart';

class StartBinding extends Bindings {
  @override
  void dependencies() {
    Get.put(ConnectionController());
    Get.put(SSController());
  }
}
