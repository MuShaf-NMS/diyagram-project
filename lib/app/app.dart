import 'package:diyagram/app/binding/start_binding.dart';
import 'package:diyagram/app/page/page.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:get/get_navigation/get_navigation.dart';

class DiyaGram extends StatelessWidget {
  const DiyaGram({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      initialBinding: StartBinding(),
      initialRoute: "/",
      getPages: pages,
      theme: ThemeData(
        appBarTheme: const AppBarTheme(
          color: Color(0xFF037bb8),
        ),
      ),
      localizationsDelegates: GlobalMaterialLocalizations.delegates,
    );
  }
}
