import 'package:firebase_storage/firebase_storage.dart';

class ImageM {
  String uuid;
  String src;
  int like;
  int dislike;

  ImageM({
    required this.uuid,
    required this.src,
    required this.like,
    required this.dislike,
  });

  Future<String> getUrl() async {
    return await FirebaseStorage.instance.ref("/images/$src").getDownloadURL();
  }

  factory ImageM.fromJson(Map<String, dynamic> json) {
    return ImageM(
        uuid: json["uuid"],
        src: json["src"],
        like: json["like"],
        dislike: json["dislike"]);
  }

  Map<String, dynamic> toJson() {
    return {
      "uuid": uuid,
      "src": src,
      "like": like,
      "dislike": dislike,
    };
  }
}

List<ImageM> parseImages(List json) {
  return json.map((e) => ImageM.fromJson(e)).toList();
}
