import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:diyagram/app/controller/comment_controller.dart';
import 'package:diyagram/app/util/date.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Comment extends GetView<CommentController> {
  const Comment({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Comment"),
      ),
      body: Obx(
        () => Stack(
          alignment: Alignment.bottomCenter,
          children: [
            Container(
              padding: const EdgeInsets.only(bottom: 75),
              child: StreamBuilder(
                stream: controller.postRef.value.snapshots(),
                builder: (BuildContext context,
                    AsyncSnapshot<DocumentSnapshot> snapshot) {
                  if (snapshot.hasData) {
                    DocumentSnapshot doc = snapshot.data!;
                    DateTime createdAt = DateTime.fromMillisecondsSinceEpoch(
                      doc["createdAt"].millisecondsSinceEpoch,
                    );
                    return ListView(
                      children: [
                        Container(
                          padding: const EdgeInsets.only(
                            top: 10,
                            left: 5,
                            right: 5,
                          ),
                          alignment: Alignment.topRight,
                          child: Text(
                            date.format(createdAt),
                            style: TextStyle(color: Colors.grey[700]),
                            textAlign: TextAlign.right,
                          ),
                        ),
                        Container(
                          padding: const EdgeInsets.symmetric(
                            horizontal: 5,
                            vertical: 10,
                          ),
                          alignment: Alignment.topLeft,
                          child: Text(
                            doc["deskripsi"],
                            textAlign: TextAlign.justify,
                          ),
                        ),
                        doc["src"] != ""
                            ? Image.network(
                                doc["src"],
                                loadingBuilder: (BuildContext contex,
                                    Widget child, ImageChunkEvent? loading) {
                                  if (loading == null) {
                                    return child;
                                  }
                                  return Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Image.asset("images/blank.jpg"),
                                      const CircularProgressIndicator(
                                        color: Color(0xFF037bb8),
                                      ),
                                    ],
                                  );
                                },
                                errorBuilder: (BuildContext context,
                                    Object exception, StackTrace? stackTrace) {
                                  return Icon(
                                    Icons.error,
                                    size: 50,
                                    color: Colors.grey[700],
                                  );
                                },
                              )
                            : Container(
                                height: 0,
                              ),
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Row(
                              children: [
                                Text(doc["like"].toString()),
                                IconButton(
                                  onPressed: () {
                                    controller.like();
                                  },
                                  icon: const Icon(Icons.thumb_up),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(doc["dislike"].toString()),
                                IconButton(
                                  onPressed: () {
                                    controller.dislike();
                                  },
                                  icon: const Icon(Icons.thumb_down),
                                ),
                              ],
                            ),
                            Row(
                              children: [
                                Text(doc["comments"].toString()),
                                IconButton(
                                  onPressed: () {
                                    controller.focusComment();
                                  },
                                  icon: const Icon(Icons.comment),
                                ),
                              ],
                            ),
                          ],
                        ),
                        StreamBuilder(
                          stream: controller.postRef.value
                              .collection("comments")
                              .orderBy("createdAt", descending: true)
                              .snapshots(),
                          builder:
                              (context, AsyncSnapshot<QuerySnapshot> snapshot) {
                            if (snapshot.hasData) {
                              return ListView(
                                  padding: const EdgeInsets.symmetric(
                                    horizontal: 5,
                                  ),
                                  shrinkWrap: true,
                                  children: snapshot.data!.docs.map((c) {
                                    DateTime createdAt =
                                        DateTime.fromMillisecondsSinceEpoch(
                                      c["createdAt"].millisecondsSinceEpoch,
                                    );
                                    return Container(
                                      padding: const EdgeInsets.only(top: 5),
                                      decoration: const BoxDecoration(
                                        border: Border(
                                          top: BorderSide(
                                            color: Colors.black,
                                          ),
                                        ),
                                      ),
                                      child: Column(
                                        crossAxisAlignment:
                                            CrossAxisAlignment.start,
                                        children: [
                                          Text(
                                            "${c["comment"]}",
                                            textAlign: TextAlign.justify,
                                          ),
                                          Container(
                                            alignment: Alignment.bottomRight,
                                            child: Text(
                                              date.format(createdAt),
                                              style: TextStyle(
                                                  color: Colors.grey[700]),
                                              textAlign: TextAlign.right,
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                  }).toList());
                            }
                            return Container();
                          },
                        ),
                      ],
                    );
                  }
                  return Container();
                },
              ),
            ),
            Container(
              padding: const EdgeInsets.symmetric(
                vertical: 5,
                horizontal: 5,
              ),
              color: Colors.white,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: TextFormField(
                      controller: controller.commentBox,
                      focusNode: controller.commentFocus,
                      decoration: InputDecoration(
                        fillColor: Colors.grey,
                        focusColor: Colors.grey,
                        hoverColor: Colors.grey,
                        hintText: "Comment",
                        // suffixIcon: IconButton(
                        //   icon: const Icon(Icons.send),
                        //   onPressed: controller.comment,
                        // ),
                        focusedBorder: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                        border: OutlineInputBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      keyboardType: TextInputType.multiline,
                      minLines: 1,
                      maxLines: 3,
                    ),
                  ),
                  Container(
                    padding: const EdgeInsets.symmetric(vertical: 5),
                    child: IconButton(
                      icon: const Icon(Icons.send),
                      onPressed: controller.comment,
                      alignment: Alignment.center,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
