import 'package:diyagram/app/controller/post_controller.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

class Post extends GetView<PostController> {
  const Post({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("Posting"),
      ),
      body: Obx(() => Stack(
            alignment: Alignment.bottomCenter,
            children: [
              Container(
                padding: const EdgeInsets.only(bottom: 75),
                height: double.maxFinite,
                // alignment: Alignment.topCenter,
                child: controller.file.value.path != ""
                    ? Stack(
                        alignment: Alignment.center,
                        children: [
                          Container(
                              alignment: Alignment.topCenter,
                              child: Image.file(controller.file.value)),
                          controller.onPost.value
                              ? Column(
                                  mainAxisSize: MainAxisSize.min,
                                  children: [
                                    CircularProgressIndicator(
                                      value: controller.progress.value,
                                      strokeWidth: 7,
                                    ),
                                    Text(
                                      (controller.progress.value * 100)
                                              .toStringAsFixed(2) +
                                          "%",
                                      style: const TextStyle(
                                        fontWeight: FontWeight.bold,
                                        fontSize: 15,
                                      ),
                                    )
                                  ],
                                )
                              : Container(),
                        ],
                      )
                    : Container(
                        alignment: Alignment.topCenter,
                        child: Image.asset("images/blank.jpg")),
              ),
              Container(
                // alignment: Alignment.bottomCenter,
                padding: const EdgeInsets.symmetric(
                  vertical: 5,
                  horizontal: 5,
                ),
                color: Colors.white,
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: [
                    Expanded(
                      child: TextFormField(
                        controller: controller.deskripsi,
                        focusNode: controller.deskripsiFocus,
                        textAlignVertical: TextAlignVertical.bottom,
                        decoration: InputDecoration(
                          fillColor: Colors.grey,
                          focusColor: Colors.grey,
                          hoverColor: Colors.grey,
                          hintText: "Catatan",
                          alignLabelWithHint: true,
                          suffixIcon: Container(
                            height: 97,
                            width: 100,
                            alignment: Alignment.bottomCenter,
                            child: Row(
                              mainAxisSize: MainAxisSize.min,
                              children: [
                                IconButton(
                                  padding: EdgeInsets.zero,
                                  icon: const Icon(Icons.image),
                                  onPressed: controller.useGallery,
                                ),
                                IconButton(
                                  padding: EdgeInsets.zero,
                                  icon: const Icon(Icons.camera_alt),
                                  onPressed: controller.useCamera,
                                )
                              ],
                            ),
                          ),
                          focusedBorder: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                          border: OutlineInputBorder(
                            borderRadius: BorderRadius.circular(30),
                          ),
                        ),
                        keyboardType: TextInputType.multiline,
                        minLines: 1,
                        maxLines: 3,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.symmetric(vertical: 5),
                      child: IconButton(
                        icon: const Icon(Icons.send),
                        onPressed: controller.upload,
                        alignment: Alignment.center,
                      ),
                    ),
                  ],
                ),
              ),
            ],
          )),
    );
  }
}
