import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:diyagram/app/controller/home_controller.dart';
import 'package:diyagram/app/util/date.dart';
import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class Home extends GetView<HomeController> {
  const Home({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: BottomNavigationBar(
        // fixedColor: Colors.black,
        selectedItemColor: Colors.black,
        unselectedItemColor: Colors.black,
        items: const [
          BottomNavigationBarItem(icon: Icon(Icons.home), label: "Home"),
          BottomNavigationBarItem(icon: Icon(Icons.search), label: "Search"),
          BottomNavigationBarItem(icon: Icon(Icons.play_arrow), label: "Video"),
        ],
      ),
      appBar: AppBar(
        title: const Text("Diyagram"),
        actions: [
          IconButton(
            onPressed: () => Get.toNamed("/post"),
            icon: const Icon(Icons.add),
          )
        ],
      ),
      body: Column(
        children: [
          Expanded(
              child: StreamBuilder(
            stream: controller.postSnapshots,
            builder: (context, AsyncSnapshot<QuerySnapshot> snapshot) {
              if (!snapshot.hasData) {
                return Container();
              }
              return ListView(
                children: snapshot.data!.docs.map(
                  (post) {
                    DateTime createdAt = DateTime.fromMillisecondsSinceEpoch(
                      post["createdAt"].millisecondsSinceEpoch,
                    );
                    return Container(
                      decoration: const BoxDecoration(
                        border: Border(
                          bottom: BorderSide(color: Colors.black),
                        ),
                      ),
                      child: Column(
                        children: [
                          Container(
                            padding: const EdgeInsets.only(
                              top: 10,
                              left: 5,
                              right: 5,
                            ),
                            alignment: Alignment.topRight,
                            child: Text(
                              date.format(createdAt),
                              style: TextStyle(color: Colors.grey[700]),
                              textAlign: TextAlign.right,
                            ),
                          ),
                          Container(
                            padding: const EdgeInsets.only(
                              bottom: 10,
                              left: 5,
                              right: 5,
                            ),
                            alignment: Alignment.topLeft,
                            child: Text(
                              post["deskripsi"],
                              textAlign: TextAlign.justify,
                            ),
                          ),
                          post["src"] != ""
                              ? Image.network(post["src"], loadingBuilder:
                                  (BuildContext contex, Widget child,
                                      ImageChunkEvent? loading) {
                                  if (loading == null) {
                                    return child;
                                  }
                                  return Stack(
                                    alignment: Alignment.center,
                                    children: [
                                      Image.asset("images/blank.jpg"),
                                      const CircularProgressIndicator(
                                        color: Color(0xFF037bb8),
                                      ),
                                    ],
                                  );
                                }, errorBuilder: (BuildContext context,
                                  Object exception, StackTrace? stackTrace) {
                                  return Icon(
                                    Icons.error,
                                    size: 50,
                                    color: Colors.grey[700],
                                  );
                                })
                              : Container(),
                          Row(
                            mainAxisAlignment: MainAxisAlignment.spaceAround,
                            children: [
                              Row(
                                children: [
                                  Text(post["like"].toString()),
                                  IconButton(
                                    onPressed: () {
                                      controller.like(post);
                                    },
                                    icon: const Icon(Icons.thumb_up),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(post["dislike"].toString()),
                                  IconButton(
                                    onPressed: () {
                                      controller.dislike(post);
                                    },
                                    icon: const Icon(Icons.thumb_down),
                                  ),
                                ],
                              ),
                              Row(
                                children: [
                                  Text(post["comments"].toString()),
                                  IconButton(
                                    onPressed: () {
                                      Get.toNamed("/comment",
                                          arguments: [post]);
                                    },
                                    icon: const Icon(Icons.comment),
                                  ),
                                ],
                              ),
                            ],
                          ),
                        ],
                      ),
                    );
                  },
                ).toList(),
              );
            },
          )),
        ],
      ),
    );
  }
}
