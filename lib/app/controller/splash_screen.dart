import 'dart:async';

import 'package:diyagram/app/controller/connection.dart';
import 'package:get/get.dart';

class SSController extends GetxController {
  ConnectionController connectionController = ConnectionController();
  bool loading = true;

  @override
  void onReady() async {
    Timer(
      const Duration(seconds: 3),
      () async {
        while (loading) {
          if (await connectionController.checkConnection()) {
            loading = false;
            Get.offAndToNamed("/home");
          }
        }
      },
    );
    super.onReady();
  }
}
