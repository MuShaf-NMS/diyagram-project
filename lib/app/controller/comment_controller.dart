import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/widgets.dart';
import 'package:get/get.dart';

class CommentController extends GetxController {
  FirebaseFirestore ff = FirebaseFirestore.instance;
  Stream<QuerySnapshot<Map<String, dynamic>>> commentSnapshots =
      FirebaseFirestore.instance
          .doc("posts/" + Get.arguments[0].id)
          .collection("comments")
          .orderBy("createdAt", descending: true)
          .snapshots();
  TextEditingController commentBox = TextEditingController();
  FocusNode commentFocus = FocusNode();
  Rx<DocumentReference> postRef =
      FirebaseFirestore.instance.doc("posts/" + Get.arguments[0].id).obs;

  like() async {
    await postRef.value.update({"like": FieldValue.increment(1)});
  }

  dislike() async {
    await postRef.value.update({"dislike": FieldValue.increment(1)});
  }

  focusComment() {
    commentFocus.requestFocus();
  }

  comment() async {
    await postRef.value
        .collection("comments")
        .add({"comment": commentBox.text, "createdAt": DateTime.now().toUtc()});
    await postRef.value.update({"comments": FieldValue.increment(1)});
    commentBox.clear();
    commentFocus.unfocus();
  }
}
