import 'dart:io';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';

class PostController extends GetxController {
  Rx<File> file = File("").obs;
  FirebaseFirestore ff = FirebaseFirestore.instance;
  CollectionReference posts = FirebaseFirestore.instance.collection("posts");
  Reference postRef = FirebaseStorage.instance.ref("images");
  FocusNode deskripsiFocus = FocusNode();
  TextEditingController deskripsi = TextEditingController();
  Rx<double> progress = 0.00.obs;
  RxBool onPost = false.obs;
  UploadTask? task;

  Future posting(String uuid, String src) async {
    await posts.add({
      "uuid": uuid,
      "src": src,
      "deskripsi": deskripsi.value.text,
      "like": 0,
      "dislike": 0,
      "comments": 0,
      "createdAt": DateTime.now().toUtc(),
    });
    deskripsi.text = "";
    onPost.value = false;
    Get.back();
  }

  Future upload() async {
    String uuid = const Uuid().v4();
    if (file.value.path == "" && deskripsi.value.text == "") {
      Get.snackbar("Gagal", "Isi minimal salah satu gambar atau catatan");
    } else if (file.value.path != "") {
      onPost.value = true;
      String ext = file.value.path.split(".").last;
      String filename = "$uuid.$ext";
      task = postRef.child(filename).putFile(file.value);
      task!.snapshotEvents.listen((event) async {
        progress.value = (event.bytesTransferred / event.totalBytes);
        if (event.state == TaskState.success) {
          String src = await postRef.child(filename).getDownloadURL();
          await posting(uuid, src);
        }
      });
    } else {
      onPost.value = true;
      await posting(uuid, "");
    }
  }

  useGallery() async {
    ImagePicker picker = ImagePicker();
    XFile? result = await picker.pickImage(
      source: ImageSource.gallery,
      imageQuality: 75,
    );
    if (result != null) {
      file.value = File(result.path);
    }
  }

  useCamera() async {
    ImagePicker picker = ImagePicker();
    XFile? result = await picker.pickImage(
      source: ImageSource.camera,
      imageQuality: 75,
    );
    if (result != null) {
      file.value = File(result.path);
    }
  }

  @override
  void onInit() {
    deskripsiFocus.requestFocus();
    super.onInit();
  }
}
