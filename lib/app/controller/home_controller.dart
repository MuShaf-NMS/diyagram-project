import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:get/get.dart';

class HomeController extends GetxController {
  Stream<QuerySnapshot<Map<String, dynamic>>> postSnapshots = FirebaseFirestore
      .instance
      .collection("posts")
      .orderBy("createdAt", descending: true)
      .snapshots();

  like(QueryDocumentSnapshot post) async {
    await FirebaseFirestore.instance
        .doc("posts/${post.id}")
        .update({"like": FieldValue.increment(1)});
  }

  dislike(QueryDocumentSnapshot post) async {
    await FirebaseFirestore.instance
        .doc("posts/${post.id}")
        .update({"dislike": FieldValue.increment(1)});
  }

  comment(QueryDocumentSnapshot post, String comment) async {
    await FirebaseFirestore.instance
        .doc("posts/${post.id}")
        .collection("comments")
        .add({"comment": comment});
    await FirebaseFirestore.instance
        .doc("posts/${post.id}")
        .update({"comments": FieldValue.increment(1)});
  }
}
